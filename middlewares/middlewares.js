const bodyParser = require('body-parser')
var cors = require('cors')
module.exports = {'applyMiddlewares':(app) => {
    app.use(cors());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
}}