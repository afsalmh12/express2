const db =  require('./mongodb_config')
const server = require('./server_config')
const middlewares = require('../middlewares/middlewares')
const startServer = (app) => {
    middlewares.applyMiddlewares(app)
    server.connect(app)
    
}
module.exports = {startServer}