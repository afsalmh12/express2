
const error_handler = require('../utils/error-handler')
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
require('dotenv').config();
const saltRounds = 10;



var verifyJwtToken = (req, res, next) => {
    var token;
    //get token from header
    token = req.headers['authorization'];
    if (!token)
        return res.status(403).send(error_handler.raiseError(401));
    else {
        jwt.verify(token, process.env.JWT_SUPER_KEY, (err, decoded) => {
            if (err)
                return res.status(403).send(error_handler.raiseError(401));
            else {
                req._id = decoded.email;
                next();
            }
        })
    }
}

var getPasswordHash = (password) => {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, saltRounds)
            .then((hash) => { resolve(hash); })
            .catch((err) => { reject(err) })
    })
}

var generateToken = (data) => {
    return new Promise((resolve, reject) => {
        resolve(jwt.sign({ email: data }, process.env.JWT_SUPER_KEY, {
            expiresIn: 86400 // expires in 1 day
        }))
    })
}

//function to verify password
var verifyHash = (hash, password) => {
    return new Promise(function (resolve, reject) {
        bcrypt.compare(password, hash)
            .then((res) => { resolve(res) })
            .catch((err) => { reject(err) })
    });
}


module.exports = {
    "getPasswordHash": getPasswordHash,
    "generateToken": generateToken,
    "verifyHash": verifyHash,
    "verifyJwtToken": verifyJwtToken
}
