const router = require('express').Router()
const auth = require('../../controllers/user/auth-controller')
const user = require('../../controllers/user/user-controller')
router.post('/login', auth.post);
router.post('/signup', user.post);
module.exports = router