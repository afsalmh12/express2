
const auth = require('./user/auth-route.js')
const user = require('./user/user-route')
const jwt = require('../utils/jwt-handler')
const end_points = (app) => {
    app.use('/auth', auth)
    app.use('/user',jwt.verifyJwtToken,user)
}
module.exports = {'registerEndPoints':end_points}
