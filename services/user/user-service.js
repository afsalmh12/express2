const user = require('../../models/user/user-model');

//function to check the given email is exist
const verifyEmail =(emailId) => {
    return new Promise(function (resolve, reject) {
        user.findOne({ email: emailId }, '_id email').then((data) => {
            resolve(data);
        }).catch((err) => {
            console.log(err);
            reject(err);
        })
    })
}


//function to save user detials in db
const addUser = (userMdl) => {
    return new Promise(function (resolve, reject) {
        let newUser = new user(userMdl);
        newUser.save()
            .then((data) => {
                resolve(data);
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            })
    })
}

const profile = (email) => {
    return new Promise(function (resolve, reject) {
        user.findOne({ email: email },{password: 0}).then((data) => {
            resolve(data);
        }).catch((err) => {
            console.log(err);
            reject(err);
        })
    })
}

module.exports = {
    verifyEmail,
    addUser,
    profile
};