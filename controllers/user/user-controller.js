const user_service = require('../../services/user/user-service')
const error_handler = require('../../utils/error-handler')
const jwt_handler = require('../../utils/jwt-handler')
const get = async (req, res) => {
    res.json(await user_service.profile(req._id))
}
const post = async (req, res) => {
    let data = req.body;
    if (data && data.email && data.password) {
        let user = await user_service.verifyEmail(data.email);
        if (!user) {
            data.password = await jwt_handler.getPasswordHash(data.password);
            user_service.addUser(data).then((result) => {
                jwt_handler.generateToken(result.email).then((token) => {
                    res.json({ token });
                }).catch((err) => {
                    res.status(500).send(error_handler.raiseError(500));
                })

            })
        } else {
            res.status(409).send("email already exist");
        }
    } else {
        res.status(400).send(error_handler.raiseError(400))
    }
}


module.exports = {
    post,
    get
}