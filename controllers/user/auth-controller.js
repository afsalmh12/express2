const error_handler = require('../../utils/error-handler')
const auth_service = require('../../services/user/auth-service')
const jwt_handler = require('../../utils/jwt-handler')
const post = async (req, res) => {
    const email = req.body ? req.body.email : undefined;
    const password = req.body ? req.body.password : undefined;
    // let validUser = false;
    if (email && password) {
        const user = await auth_service.login(email);
        if (user) {
            const validUser = await jwt_handler.verifyHash(user.password, password);
            if (validUser) {
                const token = await jwt_handler.generateToken(email);
                token ? res.json({ token: token }) : res.status(500).send(error_handler.raiseError(500))
            }
        } else {
            res.status(401).send(error_handler.raiseError(401));
        }
    } else {
        res.status(400).send(error_handler.raiseError(400));
    }
}

module.exports = {
    post
}