const express =  require('express');
const end_points = require('./routes/routes');
const configs =  require('./configs/configs');
var app = express();
configs.startServer(app)
end_points.registerEndPoints(app)

