const mongoose = require('mongoose');
const schema = mongoose.Schema;
//defining user schema
const userSchema = new schema({
    name: String,
    email:String,
    password:String
},{
    versionKey: false 
})
//exporting user schema
module.exports = mongoose.model("user",userSchema,"user");
